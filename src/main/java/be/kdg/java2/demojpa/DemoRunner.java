package be.kdg.java2.demojpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceUnit;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Stream;

@Component
public class DemoRunner implements CommandLineRunner {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void run(String... args) throws Exception {
        Book book = new Book("lord of the rings", 1000,
                LocalDate.now(), LocalTime.of(12, 00), Genre.FANTASY);
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(book);
        System.out.println(book);
        em.getTransaction().commit();
        em.close();
        //Let's insert 100 books into the database!
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Stream.generate(Book::randomBook).limit(100).forEach(
                entityManager::persist
        );
        entityManager.getTransaction().commit();
        entityManager.close();
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        book = em.find(Book.class, 20);//we find Book with ID 20
        System.out.println("Found book: " + book);
        System.out.println("Changing the number of pages:");
        book.setPages(1);
        em.getTransaction().commit();
        em.close();
        //Query using JPA Query Language:
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Book> books = em.createQuery("select b from Book b where b.title like 'title1%'")
                .getResultList();
        em.getTransaction().commit();
        em.close();
        books.forEach(System.out::println);
        //Let's delete a book:
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        book = em.find(Book.class, 1);
        em.remove(book);
        em.getTransaction().commit();
        em.close();
    }
}
