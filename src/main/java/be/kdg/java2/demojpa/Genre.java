package be.kdg.java2.demojpa;

public enum Genre {
    FANTASY, THRILLER, COMEDY
}
